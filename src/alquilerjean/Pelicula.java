/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilerjean;

/**
 *
 * @author USUARIO
 */
public class Pelicula implements IImprimir{
    private String Tipo;
    private String Nombre;
    private double Precio;
    private String Duracion;
    private int Identificador;

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }

    public String getDuracion() {
        return Duracion;
    }

    public void setDuracion(String Duracion) {
        this.Duracion = Duracion;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getIdentificador() {
        return Identificador;
    }

    public void setIdentificador(int Identificador) {
        this.Identificador = Identificador;
    }

    @Override
    public void Imprimir() {
        System.out.println(this.getTipo() + " " + this.getNombre() + " " + this.getPrecio()+ " " + this.getDuracion() + " " + this.getIdentificador());
    }
}