/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilerjean;

import java.util.List;

/**
 *
 * @author USUARIO
 */
public class Cliente implements IImprimir{
    private String Nombre;
    private String Apellido;
    private String Cedula;
    private String Telefono;
    private String Correo;

    public Cliente(String Nombre, String Apellido, String Cedula, String Telefono, String Correo) {
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Cedula = Cedula;
        this.Telefono = Telefono;
        this.Correo = Correo;
    }
    List<PeliculaFavorita> ListaFavorita;
    
    public void AgregarReserva(PeliculaFavorita NuevaPelicula)
    {
        ListaFavorita.add(NuevaPelicula);
    }
    
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }  

    @Override
    public void Imprimir() {
        System.out.println(this.getNombre() + " " + this.getApellido() + " " + this.getCedula() + " " + this.getTelefono() + " " + this.getCorreo());
    }
}