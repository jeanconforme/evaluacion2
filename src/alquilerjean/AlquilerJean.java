/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilerjean;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author USUARIO
 */
public class AlquilerJean {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese los datos del cliente");
        System.out.println("Nombre - Apellido - Telefono - Direccion - Cedula");
        Cliente cliente = new Cliente(scanner.nextLine(), scanner.nextLine(), scanner.nextLine(), scanner.nextLine(), scanner.nextLine());
        
        Pelicula viernes13 = new Pelicula ();
        viernes13.setNombre("Viernes13");
        viernes13.setPrecio(2.00);
        viernes13.setTipo("Horror");
        viernes13.setDuracion("2 horas");
        viernes13.setIdentificador(11);
        
        Pelicula starwars = new Pelicula ();
        starwars.setNombre("StarWars Episodio III");
        starwars.setPrecio(1.50);
        starwars.setTipo("Ciencia Ficcion");
        starwars.setDuracion("2 horas");
        starwars.setIdentificador(22);
        
        Pelicula rambo = new Pelicula ();
        rambo.setNombre("Rambo Lastblood");
        rambo.setPrecio(2.50);
        rambo.setTipo("Accion");
        rambo.setDuracion("2 horas");
        rambo.setIdentificador(33);
        
        Pelicula matrix = new Pelicula ();
        matrix.setNombre("Matrix");
        matrix.setPrecio(3.00);
        matrix.setTipo("Ciencia Ficcion");
        matrix.setDuracion("2 horas");
        matrix.setIdentificador(44);
        
        Pelicula rocky = new Pelicula ();
        rocky.setNombre("Rocky");
        rocky.setPrecio(2.00);
        rocky.setTipo("Accion");
        rocky.setDuracion("1:30 horas");
        rocky.setIdentificador(55);
        
        PeliculaFavorita pelicula1 = new PeliculaFavorita(viernes13);
        PeliculaFavorita pelicula2 = new PeliculaFavorita(starwars);
        PeliculaFavorita pelicula3 = new PeliculaFavorita(rambo);
        PeliculaFavorita pelicula4 = new PeliculaFavorita(matrix);
        PeliculaFavorita pelicula5 = new PeliculaFavorita(matrix);
        
        Cliente JeanPool = new Cliente(null, null, null, null, null);
        JeanPool.setApellido("Conforme");
        JeanPool.setNombre("Jean Pool");
        JeanPool.setCedula("1112223334");
        
        JeanPool.ListaFavorita = new ArrayList<>();
        JeanPool.ListaFavorita.add(pelicula1);
        JeanPool.ListaFavorita.add(pelicula2);
        JeanPool.ListaFavorita.add(pelicula3);
        
        RegistroPelicula registro1 = new RegistroPelicula(cliente, viernes13);
        RegistroPelicula registro2 = new RegistroPelicula(cliente, starwars);
        RegistroPelicula registro3 = new RegistroPelicula(cliente, rambo);
        RegistroPelicula registro4 = new RegistroPelicula(cliente, matrix);
        RegistroPelicula registro5 = new RegistroPelicula(cliente, rocky);
        
        registro1.ImprimirDatos();
        registro2.ImprimirDatos();
        registro3.ImprimirDatos();
        registro4.ImprimirDatos();
        registro5.ImprimirDatos();
    }}