/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alquilerjean;

/**
 *
 * @author USUARIO
 */
public class RegistroPelicula {
    
    private Cliente ClientePelicula;
    private Pelicula PeliculaAlquilada;

    public RegistroPelicula(Cliente ClientePelicula, Pelicula PeliculaAlquilada) {
        this.ClientePelicula = ClientePelicula;
        this.PeliculaAlquilada = PeliculaAlquilada;
    }
    public Cliente getClientePelicula() {
        return ClientePelicula;
    }

    public void setClientePelicula(Cliente ClientePelicula) {
        this.ClientePelicula = ClientePelicula;
    }

    public Pelicula getPeliculaAlquilada() {
        return PeliculaAlquilada;
    }

    public void setPeliculaAlquilada(Pelicula PeliculaAlquilada) {
        this.PeliculaAlquilada = PeliculaAlquilada;
    }
    public void ImprimirDatos(){
        
        System.out.println(this.ClientePelicula.getApellido() + " " + this.ClientePelicula.getNombre());
        System.out.println(this.PeliculaAlquilada.getNombre()+ " " + this.PeliculaAlquilada.getTipo());
        
    }
}
